import { Request, Response } from 'express';

class IndexController {

    public index(req: Request, res: Response): void {
        res.render('index', {title: 'Welcome to Book app'});
    }

    public about(req: Request, res: Response): void {
        res.render('about');
    }

}

export const indexController = new IndexController();