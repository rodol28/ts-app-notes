import mongoose from 'mongoose';
import { key } from './key';
const URI = key.database.URI;

mongoose.connect(URI, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }).then(db => console.log('db is connected'))
      .catch(err => console.error(new Error(err)));