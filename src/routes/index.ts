import { Router } from 'express';
import { indexController } from '../controllers/Index.ctrl';
 
const router: Router = Router();

router.get('/', indexController.index);
router.get('/about', indexController.about);

export default router;

