import { Router } from 'express';
import { booksController } from '../controllers/Books.ctrl';

const router: Router = Router();

router.get('/', booksController.showAllBooks);
router.get('/add', booksController.addBook);
router.post('/add', booksController.saveBook);

export default router;