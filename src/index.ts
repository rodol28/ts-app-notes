import express from 'express';
import exphbs from 'express-handlebars';
import path from 'path';

// importing routes
import routesIndex from './routes';
import routesBooks from './routes/books'

// Initialization
const app = express();
import './database';

// // Settings
app.set('port', process.env.PORT || 4000);
app.set('views', path.join(__dirname, 'views'));
app.engine('.hbs', exphbs({
    layoutsDir: path.join(app.get('views'), 'layouts'),
    partialsDir: path.join(app.get('views'), 'partials'),
    extname: '.hbs',
    defaultLayout: 'main',
    helpers: require('./libs/helpers')
}));
app.set('view engine', '.hbs');

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: false}));

// Routes
app.use('/', routesIndex);
app.use('/books', routesBooks);

// static files
app.use(express.static(path.join(__dirname, 'public')));

// Start
app.listen(app.get('port'), () => {
    console.log(`Server listening on port ${app.get('port')}`);
});